/*! \file exec_decision_maker.pde
 *
 *  \brief Schrödinger's Executive Decision Maker
 *
 *  \author jjmcd
 *  \date 2012-12-27
 */

/* I/O Shield OLED classes */
#include <IOShieldOled.h>

/* Pin definitions for the Basic I/O Shield */
#include "../basic_io_board.h"

/*! Create an instance of the OLED class */
IOShieldOledClass oled;

void happyCat( int );
void deadCat( int );

/*! Initialization */
void setup()
{
  int i;
  
  oled.begin();
  
  /* Set all LEDs to output */
  /* PIN_IOLED1 through 8 are the 8 LEDs on the Basic I/O Shield */
  /* Note that this only works because the pins are contuguous (70-77) */
  for ( i=PIN_IOLED1; i<=PIN_IOLED8; i++)
  {
    pinMode(i,OUTPUT);
    digitalWrite(i,LOW);
  }
}

/*! Main program loop */
void loop()
{
  int pressed;
  int i;
  int res;
  
  pressed = 0;
  
  while ( !pressed )
    {
      oled.clear();
      message1();
      delay( 2000 );
      while ( digitalRead(PIN_IOBTN4) )
        ;
  
      oled.clear();
      message2();
      for ( i=0; i<1000; i++ )
	{
          delay(20);
	  if ( digitalRead(PIN_IOBTN4) )
	    {
	      pressed=1;
	      break;
	    }
	}
    }
  
  oled.clear();
  while ( digitalRead(PIN_IOBTN4) )
    rand();
  
  oled.clear();
  thinking();
  delay(500);
  
  res = rand();
  oled.clear();

  if ( res > (RAND_MAX/2) )
    happyCat(50);
  else
    deadCat(45);
  pressed = 0;
  while ( !pressed )
  {
    if ( digitalRead(PIN_IOBTN1) )
      pressed = 1;
    if ( digitalRead(PIN_IOBTN2) )
      pressed = 1;
    if ( digitalRead(PIN_IOBTN3) )
      pressed = 1;
    if ( digitalRead(PIN_IOBTN4) )
      pressed = 1;
  }
}

/*! Display the opening message on the screen */
void message1( void )
{
  oled.clear();
  oled.setCursor(3,1);
  oled.putString("Executive");
  oled.setCursor(1,2);
  oled.putString("Decision Maker");
  oled.setCursor(1,3);
  oled.putString("--------------");
}

/*! Display the instructions on the screen */
void message2()
{
  oled.clear();
  oled.setCursor(0,0);
  oled.putString("Your question");
  oled.setCursor(0,1);
  oled.putString("must have a yes");
  oled.setCursor(0,2);
  oled.putString("or no answer.");
  oled.setCursor(0,3);
  oled.putString("BTN4 to start");
}

/*! Silly animation while "thinking" */
void thinking()
{
  int i,j;
  
  oled.clear();
  oled.setCursor(3,1);
  oled.putString("Thinking");

  for ( j=0; j<8; j++ )
    {
      oled.displayOff();
      for ( i=0; i<4; i++ )
	{
	  digitalWrite(PIN_IOLED1+i,HIGH);
	  digitalWrite(PIN_IOLED8-i,HIGH);
	  delay(100);
	  digitalWrite(PIN_IOLED1+i,LOW);
	  digitalWrite(PIN_IOLED8-i,LOW);
	}
      oled.displayOn();
      for ( i=4; i>=0; i-- )
	{
	  digitalWrite(PIN_IOLED1+i,HIGH);
	  digitalWrite(PIN_IOLED8-i,HIGH);
	  delay(100);
	  digitalWrite(PIN_IOLED1+i,LOW);
	  digitalWrite(PIN_IOLED8-i,LOW);
	}
    } 
}

